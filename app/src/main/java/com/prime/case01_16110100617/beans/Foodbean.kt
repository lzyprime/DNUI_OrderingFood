package com.prime.case01_16110100617.beans

data class Foodbean(var food_id: String,
                    var foodname: String,
                    var intro: String,
                    var pic: String,
                    var price: String,
                    var shop_id: String,
                    var type_id: String,
                    var recommand: String)
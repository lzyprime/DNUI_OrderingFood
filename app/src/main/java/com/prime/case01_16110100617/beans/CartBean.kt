package com.prime.case01_16110100617.beans

data class CartBean(
        var item_id: String,
        var order_id: String,
        var food_id: String,
        var shop_id: String,
        var shopname: String,
        var foodname: String,
        var num: String,
        var content: String,
        var comment_time: String,
        var pic: String,
        var price: String)